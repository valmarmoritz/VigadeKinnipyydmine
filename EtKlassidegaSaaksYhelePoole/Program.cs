﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EtKlassidegaSaaksYhelePoole
{/// <summary>
/// vigade kinnipüüdmise tehnikad
/// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            int i = 100;
            for (int j = -10; j < 11; j++)
            {
                Console.Write($"i = {i}, j = {j}: ");
                try
                {
                    i++;
                    Console.WriteLine(i / j);
                }
                catch (OverflowException) { Console.WriteLine("mälu jooksis täis"); }
                catch (DivideByZeroException) { Console.WriteLine("nulliga ei tohi jagada"); }
                catch (Exception e)
                {
                    // siin tehakse seda, mis vea puhul vaja
                    Console.WriteLine($"juhtus midagi: {e.Message} ({e.GetType().Name}) source: {e.Source}");
                }
                finally
                {
                    // seda tehakse sõltumata eelnevast
                    // Console.WriteLine("paneme poe kinni");
                }
            }
        }
    }
}
